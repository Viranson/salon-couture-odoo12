# -*- coding: utf-8 -*-
{
    'name': "Gérer Mon Salon de Couture",

    'summary': """
        Module de gestion d'un salon de couture.""",

    'description': """
        Module de gestion d'un salon de couture.
    """,

    'author': "Viranson HOUNNOUVI for AfriCouture",
    'website': "https://www.afri-couture.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account'],

    # always loaded
    'data': [
        'security/salon_couture_security.xml',
        'security/ir.model.access.csv',
        'views/couturier_views.xml',
        'views/mesure_views.xml',
        'views/modele_views.xml',
        'views/commande_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
