# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import json


class SalonCouturiers(http.Controller):
    @http.route('/salon/couturiers', type='json', auth='user')
    def get_couturiers(self):
        couturiers = request.env['salon.couturier'].search([])
        couturier_list = []
        for rec in couturiers:
            vals = {
                'id': rec.id,
                'last name': rec.last_name,
                'first name': rec.first_name,
                'contact': rec.contact,
                'speciality': rec.speciality,
                'age': rec.age,
                l: rec.user_id,
                'total mesures': rec.total_mesures
            }
            couturier_list.append(vals)
        data = {'status': 200, 'response': couturier_list, 'message': 'Success'}
        return data


class SalonCouturiersCreate(http.Controller):
    @http.route('/salon/couturier/search', type='json', auth='user', methods=['POST'])
    def create_couturier(self, name):
        # vals = {}
        # for key in kwargs.keys():
        #     vals[key] = kwargs[key]
        couturier = request.env['salon.couturier'].search[('last_name', '=', name)]
        data = {'status': 200, 'response': couturier.last_name, 'message': 'Success'}
        return data


class SalonMesuresCouturiers(http.Controller):
    @http.route('/salon/couturiers/totalmesures', type='json', auth='user')
    def get_mesures_couturiers(self):
        couturiers = request.env['salon.couturier'].search([])
        mesures_couturiers = []
        for rec in couturiers:
            vals = {
                'name': rec.first_name + " " + rec.last_name,
                'speciality': rec.speciality,
                'total mesures': rec.total_mesures
            }
            mesures_couturiers.append(vals)
        data = {'status': 200, 'response': mesures_couturiers, 'message': 'Success'}
        return data


class SalonAgeClientsCouturiers(http.Controller):
    @http.route('/salon/couturiers/clients/moyenne_age', type='json', auth='user')
    def get_moy_age_clients(self):
        couturiers = request.env['salon.couturier'].search([])
        mesures_couturiers = []
        for rec in couturiers:
            vals = {
                'name': rec.first_name + rec.last_name,
                'speciality': rec.speciality,
                'moyenne_age_clients': rec.age_avg
            }
            mesures_couturiers.append(vals)
        data = {'status': 200, 'response': mesures_couturiers, 'message': 'Success'}
        return data


class SalonMesuresGenre(http.Controller):
    @http.route('/salon/total_mesures/genre', type='json', auth='user')
    def get_mesures_genre(self):
        couturiers = request.env['salon.couturier'].search([])
        mesures_couturiers = []
        for rec in couturiers:
            vals = {
                'total_mesures_hommes': rec.total_mesures_hommes,
                'total_mesures_femmes': rec.total_mesures_femmes,
                'total_mesures_enfant': rec.total_mesures_enfant,
                'total_mesures': rec.totaux_mesures
            }
            mesures_couturiers.append(vals)
        data = {'status': 200, 'response': mesures_couturiers[0], 'message': 'Success'}
        return data


class SalonCouturiersActifs(http.Controller):
    @http.route('/salon/couturiers/actifs', type='json', auth='user')
    def get_mesures_genre(self):
        couturiers = request.env['salon.couturier'].search([])
        couturiers_actifs = []
        for rec in couturiers:
            vals = {
                'couturiers_actifs': rec.list_couturiers_actifs
            }
            couturiers_actifs.append(vals)
        data = {'status': 200, 'response': couturiers_actifs[0], 'message': 'Success'}
        return data

    # @http.route('/gerer_mon_salon_de_couture/gerer_mon_salon_de_couture/objects/', auth='public')
    # def list(self, **kw):
    #     return http.request.render('gerer_mon_salon_de_couture.listing', {
    #         'root': '/gerer_mon_salon_de_couture/gerer_mon_salon_de_couture',
    #         'objects': http.request.env['gerer_mon_salon_de_couture.gerer_mon_salon_de_couture'].search([]),
    #     })
    #
    # @http.route('/gerer_mon_salon_de_couture/gerer_mon_salon_de_couture/objects/<model("gerer_mon_salon_de_couture.gerer_mon_salon_de_couture"):obj>/', auth='public')
    # def object(self, obj, **kw):
    #     return http.request.render('gerer_mon_salon_de_couture.object', {
    #         'object': obj
    #     })
