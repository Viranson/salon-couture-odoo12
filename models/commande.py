# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


# Modèle Client Hérité Modifié
class ResPartnerInherit(models.Model):
    _inherit = ['res.partner']
    age = fields.Integer(string='Age')
    mesure_ids = fields.One2many("salon.mesure", "partner_id", string="Mesures de couture")
    couturier_id = fields.Many2one("salon.couturier", string="Couturier")
    salon_invoice_ids = fields.One2many("salon.invoice", "partner_id", string="Factures de Couture")
    # commande_ids = fields.One2many("salon.commande", "partner_id", string="Commandes de Couture")


# Modèle Facture Hérité Modifié
class SalonFactureClient(models.Model):
    _name = 'salon.invoice'
    _inherit = ['account.invoice']
    _description = 'Facture Couture'

    partner_id = fields.Many2one("res.partner", string="Client")


# Modèle Commande
class SalonCommandeClient(models.Model):
    _name = 'salon.commande'
    _description = 'Commande Couture'
    _rec_name = 'name'

    name = fields.Char(string="Libellé", compute="get_commande_name", readonly=True, store=True)
    is_invoiced = fields.Boolean(default=False)
    partner_id = fields.Many2one("res.partner", string="Client", domain=[('customer', '=', True)], required=True)
    tissus = fields.Binary(string="Image Tissus")
    couturier_id = fields.Many2one("salon.couturier", string="Couturier", required=True)
    mesure_id = fields.Many2one("salon.mesure", string="Mesure", required=True)
    modele_id = fields.Many2one("salon.modele", string="Modèle", required=True)
    order_date = fields.Date(string='Date', required=True)
    unit_price = fields.Float(string='Prix', required=True)
    quantity = fields.Integer(string='Quantité', comput="calc_total_amount", required=True)
    total_price = fields.Float(string='Montant Total', required=True)

    # Fonction de calcul du montant total de la commande
    @api.multi
    @api.onchange('unit_price', 'quantity')
    def calc_total_amount(self):
        if not self.unit_price or not self.quantity:
            self.total_price = 0
        else:
            self.total_price = self.unit_price * self.quantity

    # Fonction pour générer le _rec_name
    @api.multi
    @api.depends('order_date', 'partner_id')
    def get_commande_name(self):
        for rec in self:
            if not rec.order_date or not rec.partner_id:
                rec.name = "Commande"
            else:
                rec.name = "Commande de " + rec.modele_id.name + " | " \
                           + rec.partner_id.name + " | " + str(rec.order_date)

    # Fonction pour générer une facture de couture à partir de la commande
    @api.multi
    def create_invoice(self):
        # self.state = 'create_invoices'
        for rec in self:
            list_of_ids = []
            # inv_line_obj = rec.env['account.invoice.line']
            vendor = rec.couturier_id.user_id
            company_id = rec.env['res.users'].browse(1).company_id
            currency_value = company_id.currency_id.id
            inv_obj = rec.env['account.invoice']
            quantity = rec.quantity
            price_unit = rec.unit_price
            if rec.is_invoiced:
                raise Warning('Action Impossible. Facture déja existante!')
            else:
                journal_id = rec.env['account.journal'].search([('type', '=', 'sale')])
                if not journal_id:
                    journal_id = 1
                inv_data = {
                    'name': "Facture de Couture",
                    'reference': False,
                    'account_id': vendor.property_account_payable_id.id or False,
                    'partner_id': rec.partner_id.id,
                    'currency_id': currency_value,
                    'journal_id': journal_id and journal_id[0].id or False,
                    'origin': "Salon de Couture",
                    'payment_term_id': False,
                    'date_invoice': datetime.date.today(),
                    'team_id': False,
                    'type': 'out_invoice',
                    'company_id': company_id.id,
                    'number': 'Facture Couture/{}/{}'.format(datetime.date.today().year,
                                                             len(inv_obj.sudo().search([])) + 1),
                    'invoice_line_ids': [(0, 0, {
                        'name': rec.name,
                        'origin': 'Commande couture',
                        'account_id': vendor.property_account_payable_id.id,
                        'price_unit': price_unit,
                        'quantity': quantity,
                        'uom_id': False,
                    })],
                }
                inv_id = inv_obj.create(inv_data)
                list_of_ids.append(inv_id.id)
                if list_of_ids:
                    imd = rec.env['ir.model.data']
                    imp_req_obj_brw = rec.browse(rec._context.get('active_id'))
                    imp_req_obj_brw.write({'is_invoiced': True})
                    rec.is_invoiced = True
                    action = imd.xmlid_to_object('account.action_invoice_tree1')
                    list_view_id = imd.xmlid_to_res_id('account.invoice_tree')
                    form_view_id = imd.xmlid_to_res_id('account.invoice_form')
                    result = {
                        'name': action.name,
                        'help': action.help,
                        'type': action.type,
                        'views': [[list_view_id, 'tree'], [form_view_id, 'form']],
                        'target': action.target,
                        'context': action.context,
                        'res_model': action.res_model,
                    }
                    if list_of_ids:
                        result['domain'] = "[('id','in',%s)]" % list_of_ids
            return result
