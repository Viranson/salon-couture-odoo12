# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime, statistics


# Modèle Couturier
class SalonCouturier(models.Model):
    _name = 'salon.couturier'
    _description = 'Couturier Record'
    _rec_name = 'first_name'

    photo = fields.Binary(string="Photo")
    last_name = fields.Char(string='Nom', required=True)
    first_name = fields.Char(string='Prénoms', required=True)
    contact = fields.Char(string='Contact', required=True)
    speciality = fields.Selection(string='Spécialité',
                                  selection=[('ENFANTS', 'ENFANTS'), ('FEMMES', 'FEMMES'),
                                             ('HOMMES', 'HOMMES'), ('MIXTE', 'MIXTE')], required=True)
    birthdate = fields.Date(string='Date de naissance', required=True)
    age = fields.Integer(string="Age", compute="calc_age")
    partner_ids = fields.One2many("res.partner", "couturier_id", string="Clients")
    user_id = fields.Many2one("res.users", string="Utilisateur")
    total_mesures = fields.Integer(string="Total des mesures prises", compute="get_total_mesures")
    total_mesures_hommes = fields.Integer(string="Total des mesures Hommes", compute="get_total_mesures_hommes")
    total_mesures_femmes = fields.Integer(string="Total des mesures Femmes", compute="get_total_mesures_femmes")
    total_mesures_enfant = fields.Integer(string="Total des mesures Enfants", compute="get_total_mesures_enfants")
    totaux_mesures = fields.Integer(string="Total des mesures", compute="get_totaux_mesures")
    age_avg = fields.Integer(string="Moyenne d'âge des clients", compute="get_partner_age_avg")
    couturier_actif = fields.Html(string="Liste des Couturiers les plus actifs ayant le plus de clients",
                                  compute="get_couturier_actif")
    clients_couturier = fields.Html(string="Liste des Clients", compute="get_clients")

    # Fontion pour identifier le(s) couturier(s) actifs i.e. ayant le plus de clients
    @api.multi
    def get_couturier_actif(self):
        for rec in self:
            commandes = (list(set(rec.env['salon.commande'].search([]))))
            rec.couturier_actif = """<div class="page">
                                      <div class="container">
                                        <div class="row">
                                            <div class="col-3 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nom et Prénoms</p></div>
                                            <div class="col-3 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nombre de Clients</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 bg-secondary" style="border: solid 0.5px #008dca; text-align: center;"><p>Aucun couturier actif pour le moment</p></div>
                                        </div>
                                      </div>
                                     </div>"""
            clients_couturiers = {}
            if len(commandes) > 0:
                for command in commandes:
                    couturier = command.couturier_id.first_name + " " + command.couturier_id.last_name
                    if couturier in clients_couturiers.keys():
                        clients_couturiers[couturier] += 1
                    else:
                        clients_couturiers[couturier] = 1
                sorted_couturiers = sorted(clients_couturiers.items(), key=lambda t: t[1], reverse=True)
                qte = sorted_couturiers[0][1]
                rec.couturier_actif = """<div class="page">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-3 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nom et Prénoms</p></div>
                                                    <div class="col-2 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nombre de Clients</p></div>
                                                </div>"""
                for elt in sorted_couturiers:
                    if elt[1] >= qte:
                        rec.couturier_actif += """<div class="page">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-3 bg-success" style="border: solid 0.5px #008dca;"><p><span>{}</span></p></div>
                                                    <div class="col-2 bg-success" style="border: solid 0.5px #008dca;"><p><span>{}</span></p></div>
                                                </div>""".format(elt[0], elt[1])

    # Fontion pour lister les clients d'un couturier
    @api.multi
    def get_clients(self):
        for rec in self:
            couturier_user = rec.user_id.id
            commandes = (list(set(rec.env['salon.commande'].search([('create_uid', '=', couturier_user)]))))
            rec.clients_couturier = """<div class="page">
                                      <div class="container">
                                        <div class="row">
                                            <div class="col-3 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nom et Prénoms</p></div>
                                            <div class="col-2 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Contact</p></div>
                                            <div class="col-2 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nombre de commandes</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-7 bg-secondary" style="border: solid 0.5px #008dca; text-align: center;"><p>Aucun client pour le moment</p></div>
                                        </div>
                                      </div>
                                     </div>"""
            clients_couturiers = {}
            if len(commandes) > 0:
                for command in commandes:
                    client = command.partner_id.name + ','
                    if command.partner_id.mobile:
                        client += command.partner_id.mobile
                    elif command.partner_id.phone:
                        client += command.partner_id.phone
                    else:
                        client += command.partner_id.email

                    if client not in clients_couturiers.keys():
                        clients_couturiers[client] = 1
                    else:
                        clients_couturiers[client] += 1
                sorted_clients = sorted(clients_couturiers.items(), key=lambda t: t[1], reverse=True)
                rec.clients_couturier = """<div class="page">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-3 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nom et Prénoms</p></div>
                                                    <div class="col-2 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Contact</p></div>
                                                    <div class="col-2 bg-primary" style="border: solid 0.5px #008dca;"><p class="o_form_label o_required_modifier">Nombre de commandes</p></div>
                                                </div>"""
                for elt in sorted_clients:
                    rec.clients_couturier += """<div class="page">
                                              <div class="container">
                                                 <div class="row">
                                                    <div class="col-3 bg-info" style="border: solid 0.5px #008dca;"><p><span>{}</span></p></div>
                                                    <div class="col-2 bg-info" style="border: solid 0.5px #008dca;"><p><span>{}</span></p></div>
                                                    <div class="col-2 bg-info" style="border: solid 0.5px #008dca;"><p><span>{}</span></p></div>
                                                 </div>""".format(elt[0].split(',')[0], elt[0].split(',')[1], elt[1])

    # Fontion de calcul du total des mesures prises par un couturier
    @api.multi
    def get_total_mesures(self):
        for rec in self:
            couturier_user = rec.user_id.id
            mesures = rec.env['salon.mesure'].search([('create_uid', '=', couturier_user)])
            if len(mesures) == 0:
                rec.total_mesures = 0
            else:
                rec.total_mesures = len(mesures)

    # Fontion de calcul du total des mesures HOMMES
    @api.multi
    def get_total_mesures_hommes(self):
        mesures_hommes = self.env['salon.mesure'].search([('genre', '=', 'HOMMES')])
        for rec in self:
            if len(mesures_hommes) == 0:
                rec.total_mesures_hommes = 0
            else:
                rec.total_mesures_hommes = len(mesures_hommes)

    # Fontion de calcul du total des mesures FEMMES
    @api.multi
    def get_total_mesures_femmes(self):
        mesures_femmes = self.env['salon.mesure'].search([('genre', '=', 'FEMMES')])
        for rec in self:
            if len(mesures_femmes) == 0:
                rec.total_mesures_femmes = 0
            else:
                rec.total_mesures_femmes = len(mesures_femmes)

    # Fontion de calcul du total des mesures ENFANTS
    @api.multi
    def get_total_mesures_enfants(self):
        mesures_enfants = self.env['salon.mesure'].search([('genre', '=', 'ENFANT')])
        for rec in self:
            if len(mesures_enfants) == 0:
                rec.total_mesures_enfant = 0
            else:
                rec.total_mesures_enfant = len(mesures_enfants)

    # Fontion de calcul du total de toutes les mesures
    @api.multi
    def get_totaux_mesures(self):
        total_mesures = self.env['salon.mesure'].search([])
        for rec in self:
            if len(total_mesures) == 0:
                rec.total_mesures = 0
            else:
                rec.totaux_mesures = len(total_mesures)

    # Fontion de calcul de la moyenne d'âge des clients d'un couturier
    @api.multi
    def get_partner_age_avg(self):
        for rec in self:
            rec.age_avg = 0
            couturier_user = rec.user_id.id
            commandes = (list(set(rec.env['salon.commande'].search([('create_uid', '=', couturier_user)]))))
            if len(commandes) > 0:
                ages = []
                for command in commandes:
                    ages.append(command.partner_id.age)
                rec.age_avg = int(statistics.mean(ages))

    # Fonction de calcul de l'âge à partir de la date de naissance
    @api.multi
    @api.depends('birthdate')
    def calc_age(self):
        for rec in self:
            if not rec.birthdate:
                rec.age = 0
            else:
                today = datetime.date.today()
                rec.age = today.year - rec.birthdate.year
