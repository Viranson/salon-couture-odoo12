from odoo import models, fields, api


# Modèle 'Modèle' de couture
class SalonModele(models.Model):
    _name = 'salon.modele'
    _description = 'Modele Record'
    _rec_name = 'name'

    photo = fields.Binary(string="Photo")
    name = fields.Char(string='Nom du modèle', required=True)
    description = fields.Char(string='Description')
    type = fields.Selection(string="Type", selection=[('Barboteuse', 'Barboteuse'), ('Blazer', 'Blazer'),
                                                      ('Blouse', 'Blouse'), ('Capris', 'Capris'),
                                                      ('Chemise', 'Chemise'),
                                                      ('Combinaison', 'Combinaison'), ('Costume', 'Costume'),
                                                      ('Dessous', 'Dessous'), ('Ensemble', 'Ensemble'),
                                                      ('Haut et T-shirt', 'Haut et T-shirt'), ('Jeans', 'Jeans'),
                                                      ('Jupe', 'Jupe'), ('Maillot de bain', 'Maillot de bain'),
                                                      ('Manteau', 'Manteau'), ('Pantalon', 'Pantalon'),
                                                      ('Pulls', 'Pulls'), ('Robe', 'Robe'), ('Short', 'Short'),
                                                      ('Sweat', 'Sweat'), ('Veste', 'Veste'), ('Autre', 'Autre')])
    genre = fields.Selection(string="Genre", selection=[('FEMMES', 'FEMMES'), ('HOMMES', 'HOMMES'),
                                                        ('MIXTE', 'MIXTE')], required=True)
    materiau = fields.Char(string='Matériau du Tissus')
    origine = fields.Selection(string='Origine',
                               selection=[('AFRIQUE', 'AFRIQUE'), ('AMERIQUE', 'AMERIQUE'), ('ASIE', 'ASIE'),
                                          ('EUROPE', 'EUROPE'), ('OCEANIE', 'OCEANIE')])
    occasion = fields.Selection(string='Occasion',
                                selection=[('Anniversaire', 'Anniversaire'), ('Bureau', 'Bureau'), ('Fête', 'Fête'),
                                           ('Mariage', 'Mariage'), ('Soirée', 'Soirée'), ('Autre', 'Autre')])
    saison = fields.Selection(string="Saison", selection=[('Chaude', 'Chaude'), ('Froide', 'Froide'),
                                                          ('Beau Temps', 'Beau Temps')])
    mesure_id = fields.Many2one('salon.mesure', string="Mesure")
