from odoo import models, fields, api
import datetime


class SalonMesure(models.Model):
    _name = 'salon.mesure'
    _description = 'Mesure Record'
    _rec_name = 'name'

    # image = fields.Binary(string="Tissus")
    name = fields.Char(string="Nom", compute="get_mesure_name", readonly=True, store=True)
    genre = fields.Selection(string="Genre", selection=[('FEMMES', 'FEMMES'), ('HOMMES', 'HOMMES'),
                                                        ('ENFANT', 'ENFANT')], required=True, default="HOMMES")
    tete = fields.Char(string="Tête")
    cou = fields.Char(string="Cou")
    epaule = fields.Char(string="Epaule")
    long_bras = fields.Char(string="Longueur de bras")
    poitrine = fields.Char(string="Poitrine")
    tour_taille = fields.Char(string="Tour de taille")
    long_corps = fields.Char(string="Longueur du corps")
    hanches = fields.Char(string="Hanches")
    cuisse = fields.Char(string="Cuisse")
    genou = fields.Char(string="Genou")
    mollet = fields.Char(string="Mollet")
    cheville = fields.Char(string="Cheville")
    biceps = fields.Char(string="Biceps")
    coude = fields.Char(string="Coude")
    avant_bras = fields.Char(string="Avant-bras")
    poignet = fields.Char(string="Poignet")
    poignet_coude = fields.Char(string="Poignet-coude")
    entre_jambe = fields.Char(string="Entrejambe")
    genou_cheville = fields.Char(string="Genou-cheville")
    couture_extérieure = fields.Char(string="Couture extérieure")
    hauteur_totale = fields.Char(string="Hauteur totale")
    modele_ids = fields.One2many("salon.modele", "mesure_id", string="Modèles de vêtement")
    partner_id = fields.Many2one("res.partner", string="Client", domain=[('customer', '=', True)])
    # create_uid = fields.Integer(string="ID Utilisateur", default=lambda self: self.env.uid, readonly=True)

    @api.multi
    def get_mesure_name(self):
        for rec in self:
            rec.name = "Mesures du " + str(datetime.date.today()) + " | " \
                       + rec.env['res.partner'].search([('id', '=', rec.partner_id.id)]).name
